import QtQuick 2.12
import QtLocation 5.12
import QtPositioning 5.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtMultimedia 5.12

ApplicationWindow {
    id: page
    width: 640
    height: 480
    visible: true
    title: qsTr("Aplicativo Geoturístico")

    Plugin {
        id: mapUniversidade
        name: "osm"
        }

     Map {
        anchors.fill: parent
        plugin: mapUniversidade
        center: QtPositioning.coordinate(-1.475107, -48.456111) // UFPA
        color: "lightgreen"
        zoomLevel: 16

        MapCircle {
            id: icen
            center {
                latitude: -1.475107
                longitude: -48.456111
            }
            radius: 50  //em metros
            color: "lightgreen"
            opacity: 0.5
            border.width: 0
            MouseArea{
                anchors.fill: icen
                onClicked: {
                    icen.color = "black"
                    popicen.open()
                }
            }
        }
        Popup {
            id: popicen
            anchors.centerIn: parent
            width: page.width/2; height: page.height/2
            modal: true
            focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
            Image {
                source: "./media/icen.jpeg"
                anchors.fill: parent
            }
        }

        MapCircle {
            id: mirante
            center {
                latitude: -1.4772138
                longitude: -48.4564629
            }
            radius: 50  //em metros
            color: "lightgreen"
            opacity: 0.5
            border.width: 0
            MouseArea{
                anchors.fill: mirante
                onClicked: {
                    mirante.color = "black"
                    popmirante.open()

                }
            }
        }
        Popup {
            id: popmirante
            anchors.centerIn: parent
            width: page.width/2; height: page.height/2
            modal: true
            focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
            Video {
                id: video
                width : parent.width; height : parent.height
                source: "./media/mirante-do-rio.mp4"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        video.play()
                    }
                }
                focus: true
            }
            onClosed: {
                video.stop()
            }
        }

        MapCircle {
            id: capela
            center {
                latitude: -1.4773023
                longitude: -48.4554115
            }
            radius: 50  //em metros
            color: "lightgreen"
            opacity: 0.5
            border.width: 0
            MouseArea{
                anchors.fill: capela
                onClicked: {
                    capela.color = "black"
                    popcapela.open()
                }
            }
        }
        Popup {
            id: popcapela
            anchors.centerIn: parent
            width: page.width/2; height: page.height/2
            modal: true
            focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
            Text {text: qsTr("Texto muito lindo sobre o Mirante")}
        }
     }
}
