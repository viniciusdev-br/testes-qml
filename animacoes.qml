import QtQuick 2.12
import QtQuick.Controls 2.3
Rectangle{
    id: tela
    width: 600
    height: 600
    color: "green"
    property bool posicao: true
    Rectangle{
        id: red
        color: "red"
        width: parent.width/2
        height: parent.height/2
        Behavior on x{
            NumberAnimation {
                duration: 500
            }
        }
        Behavior on y{
            NumberAnimation {
                duration: 500
            }
        }
        MouseArea{
            anchors.fill: red
            onClicked: {
                if (posicao){
                    red.x = tela.width/2
                    red.y = tela.height/2
                    posicao = false
                }else{
                    red.x = 0
                    red.y = 0
                    posicao = true
                }
            }
        }
    }
}

