import QtQuick 2.0

Rectangle {
    width: 400; height: 400
    id: tela

    Rectangle{
        y: 0
        width: tela.width; height: tela.height/2
        color: "lightblue"
        Rectangle{
            x: tela.width/6; y: tela.height/6
            width: tela.width/6; height: tela.height/6
            color: "white"
        }
    }

    Rectangle{
        y: tela.height/2
        width: tela.width; height: tela.height/2
        color: "green"
        Rectangle{
            x: tela.width/3; y: tela.height/6
            width: tela.width/2; height: tela.height/6
            color: "blue"
        }
    }

}

