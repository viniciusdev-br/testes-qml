import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    color: mouseArea.pressed ? 'blue' : 'lightgreen'
    property int contador: 0

    Text {
        text: "Total de cliques " + contador
        anchors.centerIn: parent
        font.pointSize: 14
        font.bold: contador > 5
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: ++contador
    }
}
