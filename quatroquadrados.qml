import QtQuick 2.0

Rectangle {
    width: 300
    height: 300
    color: "lightblue"

    Rectangle{
        id: menor
        width: parent.width/2
        height: parent.height/2
        color: "lightblue"

        MouseArea{
            anchors.fill: parent
            onClicked: menor.color = "blue"
        }
    }
}
