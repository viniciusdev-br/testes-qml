import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQml 2.12

ApplicationWindow {
    id: janela
    title: qsTr("Hello World")
    width: 840
    height: 480
    visible: true

    StackView {
        id: stack
        initialItem: mainView
        anchors.fill: parent
    }


    Component {
        id: mainView
        Column {
            spacing: 0
            Rectangle{
                width: janela.width; height: botao.height + 10
                color: "lightblue"
                Row{
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 10
                    Button {
                    id: botao
                    text: "Próximo"
                    onClicked: stack.push(mainView)                    }

                    Button {
                        text: "Anterior"
                        enabled: stack.depth > 1
                        onClicked: stack.pop()
                    }

                    Text {
                    text: "Página: " + stack.depth
                    }
                }
            }
            Rectangle{
                width: janela.width; height: janela.height
                color: "lightgreen"
                Text{
                    text:"Quero fazer uma stack com duas páginas, como posso definir a segunda página"
                    color: "white"
                    font.family: "Helvetica"
                    font.pointSize: 24
                }
            }
        }

    }
}
