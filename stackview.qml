import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQml 2.12

ApplicationWindow {
    title: qsTr("Hello World")
    width: 640
    height: 480
    visible: true

    StackView {
        id: stack
        initialItem: mainView
        pushEnter: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 0
                    to:1
                    duration: 200
                }
            }
            pushExit: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 1
                    to:0
                    duration: 200
                }
            }
            popEnter: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 0
                    to:1
                    duration: 200
                }
            }
            popExit: Transition {
                PropertyAnimation {
                    property: "opacity"
                    from: 1
                    to:0
                    duration: 200
                }
            }
        anchors.fill: parent
    }

    Component {
        id: mainView

        Row {
            spacing: 40

            Button {
                text: "Próximo"
                onClicked: stack.push(mainView)
            }
            Button {
                text: "Anterior"
                enabled: stack.depth > 1
                onClicked: stack.pop()

            }
            Button{
                text: "Voltar ao topo"
                enabled: stack.depth > 1
                onClicked: pop(null)
            }

            Text {
                text: "Slide: " + stack.depth
            }
        }
    }
}
