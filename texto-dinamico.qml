import QtQuick 2.12
import QtQuick.Window 2.12

Item{
    width: meuTexto.width
    height: meuTexto.height

    Text {
        id: meuTexto
        text: "Qt Quick"
        font.family: "Helvetica"
        font.pixelSize: 50
    }
    Rectangle{
        height: 5
        width: meuTexto.width
        color: "lightgreen"
    }
}
