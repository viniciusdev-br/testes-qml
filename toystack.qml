import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQml 2.12
import QtQuick.Layouts 1.12
ApplicationWindow {
    id: janela
    title: qsTr("Hello World")
    width: 425
    height: 280
    visible: true

    StackView {
        id: stack
        initialItem: mainView
        anchors.fill: parent
    }

    ListModel{
        id: modelName
        ListElement{ name: "Universidade Federal do Pará" }
    }

    Component {
        id: mainView
        Column {
            spacing: 0
            Rectangle{
                width: janela.width; height: 50
                color: "lightblue"
                Row{
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 10
                    Button {text: "Próximo"; onClicked: stack.push(secondView)}
                    Button {text: "Anterior"; enabled: stack.depth > 1; onClicked: stack.pop()}
                    Text {text: "Página: " + stack.depth}
                }
            }
            Rectangle{
                id: body
                width: janela.width; height: janela.height
                color: "lightgreen"
                RowLayout{
                    x: 5; y: 5
                    TextField{
                        id: entradaNome
                        placeholderText : qsTr ( "Digite um nome" )
                        onAccepted: {
                            modelName.insert(0, { "name": entradaNome.text })
                            entradaNome.clear()
                        }
                    }
                }
            }
        }
    }

    Component{
        id: secondView
        Column{
            spacing: 0
            Rectangle{
                width: janela.width; height: 50
                color: "lightblue"
                Row{
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 10
                    Button {text: "Próximo"; enabled: stack.depth == 1;onClicked: stack.push(secondView)}
                    Button {text: "Anterior"; enabled: stack.depth > 1; onClicked: stack.pop()}
                    Text {text: "Página: " + stack.depth}
                }
            }
            Rectangle{
                width: janela.width; height: janela.height
                color: "lightgreen"
                Component {
                    //Delegate que controlara a forma de como os dados serão mostrados
                    id: contactDelegate
                    Item {
                        width: 200; height: 25
                        Column {
                            Text { text: '<b>Nome:</b> ' + name; font.pointSize: 18; font.family: "Helvetica"; color: "white" }
                        }
                    }
                }
                ListView{
                    anchors.fill: parent
                    model: modelName
                    delegate: contactDelegate
                    focus: true
                }
            }
        }
    }
}
